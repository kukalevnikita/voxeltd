﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {


    public float Speed;

    [HideInInspector]
    public GameObject target;
	

	void Update () {
        transform.position = Vector3.MoveTowards(transform.position, target.transform.position, Time.deltaTime * Speed);
	}
}
