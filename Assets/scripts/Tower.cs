﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour {

    public Transform shotCrystal;

    public GameObject bullet;

    public GameObject enemy;

    public float speedAtac;
    public float damage;
    [HideInInspector]
    public bool towerNowShoot = false;

	void Start () {
		
	}


    void Update() {
        if (enemy)
        {
            if (!towerNowShoot)
            {
               
                StartCoroutine(shoot());
            }
        }
	}

    IEnumerator shoot()
    {
        towerNowShoot = true;
        yield return new WaitForSeconds (speedAtac);
        GameObject bul = GameObject.Instantiate(bullet, shotCrystal.transform.position, Quaternion.identity) as GameObject;
        bullet.GetComponent<Bullet>().target = enemy;
        towerNowShoot = false;
    }
}
