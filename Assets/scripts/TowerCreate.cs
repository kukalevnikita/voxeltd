﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerCreate : MonoBehaviour {

    
    public GameObject faerTower;
    public GameObject airTower;
    public GameObject earthTower;
    public GameObject waterTower;


    public GameObject faerButtonActive;
    bool faer = false;
    public GameObject airButtonActive;
    bool air = false;
    public GameObject earthButtonActive;
    bool earth = false;
    public GameObject waterButtonActive;
    bool water = false;
    [HideInInspector]
    public GameObject curentTower;
   

    public GameObject readSpellButtonActive;
    [HideInInspector]
    public bool readSpell = false;

    void Start () {
      
	}


    void Update()
    {
        
    }

    public GameObject TowerCreater()
    {
        if (faer == true)
        {
           curentTower =  faerTower;
        }
        if (air == true)
        {
            curentTower =  airTower;
        }
        if (earth == true)
        {
            curentTower = earthTower;
        }
        if (water == true)
        {
            curentTower = waterTower;
        }
        if (water == true && faer == true) 
        {
            curentTower = earthTower;
        }
        return curentTower;
  

      
    }

    public void FaerTower()
    {
        if (faer == false)
        {
            faerButtonActive.SetActive(true);
            faer = true;
        }
        else
        {
            faerButtonActive.SetActive(false);
            faer = false;
        }
    }
    public void AirTower()
    {
        if (air == false)
        {
            airButtonActive.SetActive(true);
            air = true;
        }
        else
        {
            airButtonActive.SetActive(false);
            air = false;
        }
    }
    public void EarthTower()
    {
        if (earth == false)
        {
            earthButtonActive.SetActive(true);
            earth = true;
        }
        else
        {
            earthButtonActive.SetActive(false);
            earth = false;
        }
    }
    public void WaterTower()
    {
        if (water == false)
        {
            waterButtonActive.SetActive(true);
            water = true;
        }
        else
        {
            waterButtonActive.SetActive(false);
            water = false;
        }
    }
    public void ReadSpell()
    {
        if (water || faer || air || earth)
        {
            if (readSpell == false)
            {
                readSpellButtonActive.SetActive(true);
                readSpell = true;
            }
            else
            {
                readSpellButtonActive.SetActive(false);
                readSpell = false;
            }
        }
    }

    public void RestartGameUI()
    {
        faerButtonActive.SetActive(false);
        airButtonActive.SetActive(false);
        earthButtonActive.SetActive(false);
        waterButtonActive.SetActive(false);
        readSpellButtonActive.SetActive(false);
        faer = false;
        air = false;
        earth = false;
        water = false;
        readSpell = false;
    }

    public void X2()
    {
        Time.timeScale = 2;
    }
}
