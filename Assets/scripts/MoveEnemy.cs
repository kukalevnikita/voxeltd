﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveEnemy : MonoBehaviour
{

    public int enemySpeed;
    int n = 0;
    GameObject wayEnemy;

    void Start()
    {
        wayEnemy = GameObject.FindGameObjectWithTag("way");
    }


    void Update()
    {
        howToGoEnemy(wayEnemy.GetComponent<Spawner>().wayPointEnemy);
    }

    void howToGoEnemy(List<Transform> list1)
    {
        if (n < list1.Count)
        {
            transform.position = Vector3.MoveTowards(transform.position, list1[n].position, enemySpeed * Time.deltaTime);
            if (list1[n].CompareTag("1"))
            {
                transform.Rotate(Vector3.up * 80 * Time.deltaTime);
            }

            if (list1[n].CompareTag("2"))
            {
                transform.Rotate(Vector3.up * -80 * Time.deltaTime);
            }

            if (transform.position == list1[n].position)
            {
                n++;
            }
        }
    }
}
