﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public List<Transform> wayPointEnemy;

    public GameObject enemy;
    public Transform startPositionEnemy;
    public int howManyCreateEnemy;

    public int howManyWaveEnemy;

    int n = 0;
    


    public float timeIntervalSpawnEnemy;
    float ancillaryTimeVariable;

    void Start()
    {
        ancillaryTimeVariable = timeIntervalSpawnEnemy;

    }

    void Update()
    {
        if (n < howManyCreateEnemy)
        {
            timeIntervalSpawnEnemy -= Time.deltaTime;
            if (timeIntervalSpawnEnemy < 0)
            {
                Instantiate(enemy, startPositionEnemy.transform.position, startPositionEnemy.transform.rotation);
                timeIntervalSpawnEnemy = ancillaryTimeVariable;
                n++;
            }
        }
    }
}
