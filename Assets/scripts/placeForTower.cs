﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceForTower : MonoBehaviour {

    public GameObject signForTower;
    public GameObject placeForTower;
 
    public TowerCreate Creater;

    bool canCreatrTower = true;
   

	void Start () {
       
    }
	
	void Update () {
		
	}


    private void OnMouseDown()
    {
        
        
            if (canCreatrTower == true && Creater.readSpell == true )
            {
                signForTower.SetActive(false);
                whatTowerCreate(Creater.TowerCreater());
                canCreatrTower = false;
                Creater.RestartGameUI();
            }
        
    }

    void whatTowerCreate(GameObject curentTower)
    {
        Instantiate(curentTower, placeForTower.transform.position, placeForTower.transform.rotation);
    }


}
