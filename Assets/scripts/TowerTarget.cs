﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerTarget : MonoBehaviour {
    public GameObject tw;



    [SerializeField]
    GameObject enemy;

    public float radius = 100f;

    float pos;

    private void Start()
    {
        
    }

    private void Update()
    {
        if (enemy == null)
        {
            FindEnemy();
           
        }
        else
        {
            float currentDistance = Vector3.Distance(enemy.transform.position, transform.position);
            if (radius < currentDistance)
            {
                enemy = null;
            }
        }

    }

    void FindEnemy()
    {
        var enemies = GameObject.FindGameObjectsWithTag("enemy");
        enemy = null;
     
        
        foreach (var enem in enemies)
           
           
        {
            float currentDistance = Vector3.Distance(enem.transform.position, tw.transform.position);
            if (currentDistance <= radius)
            {
                enemy = enem;
            }
        }
        tw.GetComponent<Tower>().enemy = enemy;
    }
}
